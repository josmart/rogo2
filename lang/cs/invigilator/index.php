<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Přístup dohlížejícího';
$string['lab'] = 'Učebna:';
$string['unknownlab'] = ' - neznámá učebna';
$string['nopapersfound'] = 'Nebyly nalezeny žádné dokumenty!';
$string['nopapersfoundmsg'] = 'K této učebně nelze, v doučasnosti, dohledat žádné dokumenty.';
$string['emergencynumbers'] = 'Čísla na pohotovost';
$string['title'] = 'Nadpis';
$string['surname'] = 'Příjmení';
$string['forenames'] = 'Křestní jméno';
$string['extension_mins'] = 'Prodloužení (minuty)';
$string['endtime'] = 'Čas ukončení';
$string['extendtime'] = 'Prodloužení';
$string['extendtimeby'] = 'Prodloužení času o';
$string['addnote']        = 'Přidat poznámku';
$string['toiletbreak'] = 'Přestávka';
$string['currenttime']    = 'Aktuální čas:';
$string['start'] = 'Start:';
$string['end']       = 'Konec';
$string['start_but'] = 'Start'; 
$string['endat_but']  = 'Konec v';
$string['session_end'] = 'Konec relace';
$string['duration'] = 'Trvání:';
$string['mins'] = 'minut';
$string['hour']      = 'hodina';
$string['hours']     = 'hodiny';
$string['papernote'] = 'Poznámky dokumentu';
$string['extratime'] = 'Prodloužení';
$string['preexam'] = 'Před zkouškou';
$string['preexamlist'] = '<ol>
    <li>Place log in instructions at each workstation</li>
    <li>Place blank paper each workstation</li>
    <li>Check all students have logged in correctly</li>
    <li>Use \'Guest Login\' accounts for anyone not able to log in</li>
    <li><strong>NOTE:</strong> Do not start before scheduled start time</li>
    </ol>';
$string['midexam'] = 'V průběhu zkoušky';
$string['midexamlist'] = '<ol>
    <li>For emergency support call one of the numbers show on the paper tabs</li>
    <li>Record IT/personal problems against relevant students (click their name)</li>
    <li>Record toilet breaks against relevant students (click their name)</li>
    <li>Record general paper/question problems using \'Add Paper Note...\' button.</li>
    </ol>';
$string['postexam'] = 'Po zkoušce';
$string['postexamlist'] = '<ol>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "That is the end of the exam. Please navigate to the last screen and click \'Finish\'."</li>
    <li><img src="../artwork/green_speech.gif" width="15" height="14" /> "Click \'Close Window\' and then log out of Rog&#333."</li>
    <li>Collect up log in instructions for reuse</li>
    <li>Collect and dispose of blank paper</li>
    <li>Ensure <strong>all</strong> workstations are logged out</li>
    </ol>';
$string['time'] = 'Čas';
$string['timedexam'] = 'Zkouška s časovým limitem:';
$string['timeerror'] = 'Chyba v čase';
$string['timeerrormsg'] = 'Zkouška musí trvat minimálně %d minut.';
$string['examquestionclarifications'] = 'Ujasnění testové úlohy';
$string['midexamclarifications'] = 'Ujasnění v průběhu testování';
$string['examchecklist'] = 'Kontrolní seznam zkoušky';
$string['viewrubric'] = 'Přehled Rubriky';
$string['examrubric'] = 'Rubrika zkoušky';
$string['unknowncomputer'] = 'Unknown Computer';
$string['unknowncomputermsg'] = 'The computer you are attempting to access Rog&#333; from is not recognised.<br />Please call one of the emergency numbers for help.';
?>